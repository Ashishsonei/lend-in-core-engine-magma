package com.kuliza.lending.authorization.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakAccessDeniedHandler;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakAuthFilter;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakAuthenticationEntryPoint;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakAuthenticationProvider;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.utils.AuthConstants;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private KeyCloakAuthFilter keyCloakAuthFilter;

	@Autowired
	private KeyCloakAuthenticationEntryPoint keyCloakAuthenticationEntryPoint;

	@Autowired
	KeyCloakAuthenticationProvider keyCloakAuthenticationProvider;

	@Autowired
	KeyCloakAccessDeniedHandler keyCloakAccessDeniedHandler;

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(keyCloakAuthenticationProvider);
	}

	@Bean
	public FilterRegistrationBean registration(KeyCloakAuthFilter filter) {
		FilterRegistrationBean registration = new FilterRegistrationBean(filter);
		registration.setEnabled(false);
		return registration;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		final CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("*"));
		configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
		// setAllowCredentials(true) is important, otherwise:
		// The value of the 'Access-Control-Allow-Origin' header in the response
		// must not be the
		// wildcard '*' when the request's credentials mode is 'include'.
		configuration.setAllowCredentials(true);
		// setAllowedHeaders is important! Without it, OPTIONS preflight request
		// will fail with 403 Invalid CORS request
		configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		http.authorizeRequests().antMatchers(Constants.CE_PROD_DEPLOYMENT_ENDPOINT)
				.hasAnyRole(Constants.CE_ADMIN_USER_ROLE)
				.antMatchers(HttpMethod.GET, Constants.CE_PRODUCT_ENDPOINT, Constants.CE_PRODUCT_LIST_ENDPOINT,
						Constants.CE_PRODUCT_ALL)
				.hasAnyRole(Constants.CE_ADMIN_USER_ROLE, Constants.CE_MANAGER_USER_ROLE,
						Constants.CE_SUPERVISOR_USER_ROLE)
				.antMatchers(Constants.CE_PRODUCT_TEST_ENDPOINT)
				.hasAnyRole(Constants.CE_ADMIN_USER_ROLE, Constants.CE_MANAGER_USER_ROLE,
						Constants.CE_SUPERVISOR_USER_ROLE)
				.antMatchers(Constants.CE_PRODUCT_LIST_ALL_ENDPOINT, Constants.CE_PRODUCT_CATEGORY_ENDPOINT,
						Constants.CE_PRODUCT_CATEGORY_LIST_ENDPOINT, Constants.CE_PRODUCT_FIRE_RULES_ENDPOINT,
						Constants.CE_PRODUCT_PUBLISH_ENDPOINT)
				.hasAnyRole(Constants.CE_ADMIN_USER_ROLE, Constants.CE_SUPERVISOR_USER_ROLE)
				.antMatchers(AuthConstants.AUTH_LIST_GROUPS_ENDPOINT, AuthConstants.AUTH_CREATE_GROUPS_ENDPOINT,
						AuthConstants.AUTH_CREATE_ROLES_ENDPOINT, AuthConstants.AUTH_ADD_ROLES_TO_USER_ENDPOINT,
						AuthConstants.AUTH_LIST_USERS_ENDPOINT, AuthConstants.AUTH_LOGOUT_ACTIVE_SESSION_ENDPOINT)
				.hasAnyRole(AuthConstants.AUTH_REALM_ADMIN_USER)
				.antMatchers(AuthConstants.AUTH_LOGIN_ENDPOINT, AuthConstants.AUTH_LOGOUT_ENDPOINT,
						AuthConstants.AUTH_SIGNUP_ENDPOINT, AuthConstants.AUTH_REFRESH_TOKEN_ENDPOINT,
						Constants.JOURNEY_CUST_ENDPOINT, Constants.JOURNEY_DEV_ENDPOINT,
						Constants.JOURNEY_LOGIN_ENDPOINT, Constants.JOURNEY_OTP_ENDPOINT, Constants.CONFIG_API_ENDPOINT)
				.permitAll().antMatchers(Constants.CE_PRODUCT_API_ENDPOINTS)
				.hasAnyRole(Constants.CE_ADMIN_USER_ROLE, Constants.CE_SUPERVISOR_USER_ROLE)
				.antMatchers(Constants.JOURNEY_INITIATE_ENDPOINTS).hasAnyRole(AuthConstants.DEFAULT_USER_ROLE)
				.anyRequest().authenticated().and()
				.addFilterBefore(keyCloakAuthFilter, SecurityContextHolderAwareRequestFilter.class).exceptionHandling()
				.accessDeniedHandler(keyCloakAccessDeniedHandler)
				.authenticationEntryPoint(keyCloakAuthenticationEntryPoint).and().headers().cacheControl().disable()
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf().disable()
				.logout().disable().cors().configurationSource(source);

	}

}
