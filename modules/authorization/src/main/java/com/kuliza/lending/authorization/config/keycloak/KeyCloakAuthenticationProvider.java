package com.kuliza.lending.authorization.config.keycloak;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.kuliza.lending.authorization.service.KeyCloakManager;

@Component
public class KeyCloakAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	KeyCloakManager keyCloakManager;

	@Autowired
	KeyCloakUserDetailsService keyCloakUserDetailsService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String token = authentication.getCredentials().toString();
		String keyCloakUserId = keyCloakManager.auth(token);
		UserDetails userDetails = keyCloakUserDetailsService.loadUserByKeyCloakUserId(keyCloakUserId);
		KeyCloakAuthToken auth = new KeyCloakAuthToken(userDetails, token, userDetails.getAuthorities());
		auth.setUser_Id(keyCloakUserDetailsService.getUserIdFromRepresentation(keyCloakUserId));
		return auth;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return KeyCloakAuthToken.class.isAssignableFrom(authentication);
	}
}