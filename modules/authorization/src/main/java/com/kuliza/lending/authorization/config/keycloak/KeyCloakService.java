package com.kuliza.lending.authorization.config.keycloak;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.UserSessionRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import com.kuliza.lending.authorization.exception.ErrorMessages;
import com.kuliza.lending.authorization.exception.RoleAlreadyExistException;
import com.kuliza.lending.authorization.exception.RoleDoesNotExistException;
import com.kuliza.lending.authorization.exception.UserAlreadyExistException;
import com.kuliza.lending.authorization.exception.UserNotFoundException;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.utils.AuthConstants;

@Service
@EnableConfigurationProperties({ KeyCloakConfig.class })
public class KeyCloakService {

	private KeyCloakConfig keyCloakConfig;
	private Keycloak keycloak;
	private OpenIdService openIdService;
	private ClientRepresentation clientRepresentation;
	private RealmResource realmResource;
	private GroupsResource groupsResource;
	private ClientsResource clientsResource;
	private UsersResource usersResource;

	private static final Logger logger = LoggerFactory.getLogger(KeyCloakService.class);

	public KeyCloakService(KeyCloakConfig keyCloakConfig) {
		this.keyCloakConfig = keyCloakConfig;
		this.buildKeyCloak();
		this.buildOpenIdService();
		this.buildResources();
		this.buildClientRepresentation();
	}

	private void buildKeyCloak() {
		this.keycloak = KeycloakBuilder.builder().serverUrl(keyCloakConfig.getAuthEndpoint())
				.realm(keyCloakConfig.getRealm()).grantType(OAuth2Constants.PASSWORD)
				.clientId(keyCloakConfig.getClientId()).clientSecret(keyCloakConfig.getClientSecret())
				.username(keyCloakConfig.getAdminUsername()).password(keyCloakConfig.getAdminPassword()).build();
	}

	private void buildOpenIdService() {
		this.openIdService = this.keycloak.proxy(OpenIdService.class,
				URI.create(this.keyCloakConfig.getAuthEndpoint()));
	}

	private void buildResources() {
		this.realmResource = this.keycloak.realm(this.keyCloakConfig.getRealm());
		this.groupsResource = this.realmResource.groups();
		this.clientsResource = this.realmResource.clients();
		this.usersResource = this.realmResource.users();
	}

	private void buildClientRepresentation() {
		this.clientRepresentation = this.clientsResource.findByClientId(this.keyCloakConfig.getClientId()).get(0);
	}

	private void addClientCredentialsToForm(Form form) {
		form.param(AuthConstants.AUTH_CLIENT_ID, this.keyCloakConfig.getClientId());
		form.param(AuthConstants.AUTH_CLIENT_SECRET, this.keyCloakConfig.getClientSecret());
	}

	private Form getFormWithClientCredentials() {
		Form form = new Form();
		this.addClientCredentialsToForm(form);
		return form;
	}

	/**
	 * this method is used to grant access token for client
	 * 
	 * @param Form
	 * @return returns AccessTokenResponse
	 */
	public AccessTokenResponse grantAccessToken(Form form) {
		logger.info("--> Entering grantAccessToken()");
		this.addClientCredentialsToForm(form);
		AccessTokenResponse accessTokenResponse = null;
		try {
			logger.debug("getting token response ");
			accessTokenResponse = openIdService.grantToken(this.keyCloakConfig.getRealm(), form.asMap());
			logger.debug("Successfully generated response for getting token");
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_RESPONSE_OR_INVALID.getErrorMessage(), e);
			throw e;
		}
		logger.info("<-- Exiting from grantAccessToken()");
		return accessTokenResponse;

	}

	/**
	 * this method is used to check if provided token is active or not
	 * 
	 * @param token String
	 * @return true or false
	 */
	public boolean isTokenActive(String token) {
		logger.info("Checking for active token");
		Form form = getFormWithClientCredentials();
		form.param(AuthConstants.AUTH_TOKEN, token);
		Response response = openIdService.checkSession(this.keyCloakConfig.getRealm(), form.asMap());
		Map<String, Object> responseBody = response.readEntity(Map.class);
		logger.debug("IS token Active " + responseBody.getOrDefault("active", false).toString());
		return Boolean.parseBoolean(responseBody.getOrDefault("active", false).toString());

	}

	/**
	 * this method is used to logout user form current active session
	 * 
	 * @param Form
	 * @return logout response
	 */
	public Response logoutUser(Form form) {
		addClientCredentialsToForm(form);
		logger.info("--> Entering logoutUser()");
		Response response = null;
		try {
			response = openIdService.clearSession(this.keyCloakConfig.getRealm(), form.asMap());
		} catch (Exception e) {
			logger.error("Error while user logout ", e);
			throw e;
		}
		logger.info("<-- Exiting logoutUser()");
		return response;

	}

	// master logout
	/**
	 * this method is used to logout user from all the active sessions
	 * 
	 * @param userid
	 * @return
	 */
	public void logoutUserFromAllSessions(String keyCloakUserId) {
		logger.info("--> Entering logoutUserFromAllSessions()");
		try {
			this.usersResource.get(keyCloakUserId).logout();
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_LOGOUT_FROM_ALL_ACTIVE_SESSIONS.getErrorMessage() + " userId "
					+ keyCloakUserId, e);
			throw e;
		}
		logger.debug("Successfully logged out user from all active sessions ");
		logger.info("<-- Exiting logoutUserFromAllSessions()");
	}

	/**
	 * This method takes user detalis to create new user
	 * 
	 * @param firstName
	 * @param lastName
	 * @param username   -mandatory
	 * @param emailId    -mandatory
	 * @param password   -Mandatory
	 * @param roles
	 * @param attributes
	 * @return details of user if created else error if existing
	 */
	public UserRepresentation createUser(String firstName, String lastName, String username, String emailId,
			String password, List<String> roles, Map<String, List<String>> attributes) {
		logger.info("--> Entering createUser()");
		List<RoleRepresentation> roleRepresentations = new ArrayList<>();
		GroupRepresentation groupRepresentation = getGroupWithName(AuthConstants.DEFAULT_USER_GROUP);
		for (String role : roles) {
			try {
				logger.debug("Checking for role in keycloak for client:" + this.clientRepresentation.getName());
				RoleRepresentation roleRepresentation = this.clientsResource.get(this.clientRepresentation.getId())
						.roles().get(role).toRepresentation();
				roleRepresentations.add(roleRepresentation);
			} catch (Exception ex) {
				logger.error("No role exists: role:: " + role, ex);
				throw new RoleDoesNotExistException("no role exists.[" + role + "] please create role first");
			}
		}
		logger.debug("Creating user with emailId: " + emailId);
		UserRepresentation userRepresentation = createUserRepresentation(username, firstName, lastName, emailId);
		userRepresentation.setCredentials(Arrays.asList(this.createCredentials(password)));
		if (attributes != null && !attributes.isEmpty()) {
			logger.info("Adding custom attributes to user");
			userRepresentation.setAttributes(attributes);
		}
		Response response = usersResource.create(userRepresentation);
		if (response.getStatus() == AuthConstants.CREATED_STATUS_CODE) {
			String keyCloakUserId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
			try {
				logger.debug("Adding user to group");
				usersResource.get(keyCloakUserId).joinGroup(groupRepresentation.getId());
			} catch (Exception e) {
				logger.error("Error while adding user to group", e);
				throw e;
			}
			try {
				logger.debug("Adding roles to user");
				usersResource.get(keyCloakUserId).roles().clientLevel(this.clientRepresentation.getId())
						.add(roleRepresentations);
			} catch (Exception e) {
				logger.error("Error while adding roles to user", e);
				throw e;
			}
			logger.debug("Successfully created user with emailId: " + emailId);
			userRepresentation = usersResource.get(keyCloakUserId).toRepresentation();
		} else {
			logger.error("Request for user account creation failed with HTTP code : " + response.getStatus()
					+ " Reason " + response.getStatusInfo());
			throw new UserAlreadyExistException("user with this email id or username already registered");
		}
		logger.info("<-- Exiting createUser()");
		return userRepresentation;
	}

	public UserRepresentation createUserRepresentation(String userName, String firstName, String lastName,
			String emailId) {
		UserRepresentation userRepresentation = new UserRepresentation();
		userRepresentation.setEnabled(true);
		userRepresentation.setUsername(userName);
		if (CommonHelperFunctions.isNotEmpty(firstName))
			userRepresentation.setFirstName(firstName);
		if (CommonHelperFunctions.isNotEmpty(firstName))
			userRepresentation.setLastName(lastName);
		userRepresentation.setEmail(emailId);
		return userRepresentation;
	}

	/**
	 * This method takes user detalis to create new user
	 * 
	 * @param emailId  -mandatory
	 * @param password -Mandatory
	 * @param roles
	 * @return details of user if created else error if existing
	 */
	public UserRepresentation createUserWithRole(String emailId, String password, List<String> roles,
			Map<String, List<String>> attributes) {
		List<RoleRepresentation> roleRepresentations = new ArrayList<>();
		logger.info("--> Entering createUserWithRole()");
		GroupRepresentation groupRepresentation = getGroupWithName(AuthConstants.DEFAULT_USER_GROUP);
		for (String role : roles) {
			try {
				logger.debug("Checking for role in keycloak for client:" + this.clientRepresentation.getName());
				RoleRepresentation roleRepresentation = this.clientsResource.get(this.clientRepresentation.getId())
						.roles().get(role).toRepresentation();
				roleRepresentations.add(roleRepresentation);
			} catch (Exception ex) {
				logger.error("No role exists: role:: " + role, ex);
				throw new RoleDoesNotExistException("No role exists.");
			}
		}
		UserRepresentation userRepresentation = createUserRepresentation(emailId, null, null, emailId);
		userRepresentation.setCredentials(Arrays.asList(this.createCredentials(password)));
		if (attributes != null && !attributes.isEmpty()) {
			logger.info("Adding custom attributes to user");
			userRepresentation.setAttributes(attributes);
		}
		Response response = usersResource.create(userRepresentation);
		if (response.getStatus() == AuthConstants.CREATED_STATUS_CODE) {
			String keyCloakUserId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
			try {
				usersResource.get(keyCloakUserId).joinGroup(groupRepresentation.getId());
			} catch (Exception e) {
				logger.error("Error while adding user to group ", e);
				throw e;
			}
			try {
				usersResource.get(keyCloakUserId).roles().clientLevel(this.clientRepresentation.getId())
						.add(roleRepresentations);
			} catch (Exception e) {
				logger.error("Error while adding roles to user ", e);
				throw e;
			}
			logger.debug("Successfully created user with emailId: " + emailId);
			userRepresentation = findUserByKeyCloakUserId(keyCloakUserId);
		} else {
			logger.error("Request for user account creation failed with HTTP code : " + response.getStatus()
					+ " Reason " + response.getStatusInfo());
			throw new UserAlreadyExistException("user with this email id or username already registered");
		}
		logger.info("<-- Exiting createUserWithRole()");
		return userRepresentation;
	}

	public UserRepresentation createUserWithRole(String emailId, String password, String role) {
		return createUserWithRole(emailId, password, Arrays.asList(role),
				null);
	}
	private CredentialRepresentation createCredentials(String password) {
		CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
		credentialRepresentation.setTemporary(false);
		credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
		credentialRepresentation.setValue(password);
		return credentialRepresentation;
	}

	/**
	 * This method used to get all client roles for user
	 * 
	 * @param userId
	 * @return list of roles associated with user
	 */
	public List<String> getClientRoleListForUser(String keyCloakUserId) {
		List<String> roles = new ArrayList<>();
		try {
			roles = usersResource.get(keyCloakUserId).roles().clientLevel(this.clientRepresentation.getId())
					.listEffective().stream().map(i -> i.getName()).collect(Collectors.toList());
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_ROLES.getErrorMessage() + "userId: " + keyCloakUserId, e);
			throw e;
		}
		return roles;
	}

	// creation of role
	/**
	 * This method is used to create a role for client
	 * 
	 * @param roleNmae
	 * @return
	 */
	public void createRole(String rolename) {
		logger.info("--> Entering createRole()");
		try {
			RoleRepresentation roleRepresentation = this.clientsResource.get(this.clientRepresentation.getId()).roles()
					.get(rolename).toRepresentation();
			throw new RoleAlreadyExistException("role already exists.");
		} catch (Exception ex) {
			logger.error("Error while getting roleRepresentation ", ex);
			logger.debug("Creating new role with Name: " + rolename);
			RoleRepresentation roleRepresentationLatest = new RoleRepresentation();
			roleRepresentationLatest.setName(rolename);
			this.clientsResource.get(this.clientRepresentation.getId()).roles().create(roleRepresentationLatest);
		}
		logger.info("<-- Exiting createRole()");

	}

	public String extractKeyCloakUserIdFromAccessToken(String accessToken) {

		String[] split_string = accessToken.split("\\.");
		String base64EncodedBody = split_string[1];
		Base64 base64 = new Base64(true);
		String body = new String(base64.decode(base64EncodedBody));
		String keyCloakUserId = (new JSONObject(body)).getString("sub");
		return keyCloakUserId;
	}

	/**
	 * This method is used to find user by given userId
	 * 
	 * @param userId
	 * @return User details in userrepresentation
	 */
	public UserRepresentation findUserByKeyCloakUserId(String keyCloakUserId) {
		logger.info("Realm for keycloak is :-" + this.keyCloakConfig.getRealm());
		RealmResource realmResource = this.keycloak.realm(this.keyCloakConfig.getRealm());
		UserRepresentation representation = new UserRepresentation();
		try {
			representation = realmResource.users().get(keyCloakUserId).toRepresentation();
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_USER_WITH_UID.getErrorMessage() + keyCloakUserId, e);
			throw e;
		}
		return realmResource.users().get(keyCloakUserId).toRepresentation();
	}

	/**
	 * This method is used to find user by given emailId
	 * 
	 * @param emailId
	 * @return User details in userrepresentation
	 */
	public UserRepresentation findUserByEmail(String emailId) {
		logger.info("--> Entering findUserByEmail()");
		logger.debug("User EmailId: " + emailId);
		List<UserRepresentation> users = null;
		try {
			users = this.usersResource.search(null, null, null, emailId, null, null);
		} catch (Exception e) {
			logger.error(ErrorMessages.ERROR_FINDING_USER_WITH_EMAIL.getErrorMessage() + emailId, e);
			throw e;
		}
		if (users.size() > 0) {
			UserRepresentation user = users.stream().filter(o -> o.getEmail().equals(emailId)).findFirst().get();
			if (user != null && user.getEmail().equals(emailId)) {
				logger.info("<-- Exiting findUserByEmail()");
				return user;
			} else {
				throw new UserNotFoundException("user with emailId not found");
			}
		} else {
			logger.error("user with email id not found");
			throw new UserNotFoundException("user with email id not found");
		}
	}

	/**
	 * This method is used to find users by given role
	 * 
	 * @param roleName
	 * @return set of user representation
	 */
	public Set<UserRepresentation> findUsersByRole(String roleName) {
		logger.info("--> Entering findUsersByRole()");
		Set<UserRepresentation> users = new HashSet<>();
		try {
			users = this.clientsResource.get(this.clientRepresentation.getId()).roles().get(roleName)
					.getRoleUserMembers();
		} catch (Exception ex) {
			logger.error("Error getting roles for user ", ex);
			throw new RoleDoesNotExistException("No role exists.");
		}
		List<String> userEmails = new ArrayList<>();
		users.forEach(user -> userEmails.add(user.getEmail()));
		logger.info("<-- Exiting findUsersByRole()");
		return users;
	}

	public void enableFlagUpdate(String username, Boolean isEnabled) {
		UserRepresentation userRepresentation = this.findUserByEmail(username);
		userRepresentation.setEnabled(isEnabled);
		try {
			usersResource.get(userRepresentation.getId()).update(userRepresentation);
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_UPDATE_USER.getErrorMessage() + username, e);
			throw e;
		}
	}

	public void updateRole(String username, String role) {
		UserRepresentation userRepresentation = this.findUserByEmail(username);
		RoleRepresentation roleRepresentation = new RoleRepresentation();
		try {
			roleRepresentation = this.clientsResource.get(this.clientRepresentation.getId()).roles().get(role)
					.toRepresentation();
		} catch (Exception ex) {
			throw new RoleDoesNotExistException("No role exists.");
		}
		try {
			// previous added role to remove
			RoleRepresentation removedRoleRepresentation = usersResource.get(userRepresentation.getId()).roles()
					.clientLevel(this.clientRepresentation.getId()).listEffective().get(0);
			usersResource.get(userRepresentation.getId()).roles().clientLevel(this.clientRepresentation.getId())
					.remove(Collections.singletonList(removedRoleRepresentation));
			usersResource.get(userRepresentation.getId()).roles().clientLevel(this.clientRepresentation.getId())
					.add(Collections.singletonList(roleRepresentation));
		} catch (Exception ex) {
			usersResource.get(userRepresentation.getId()).roles().clientLevel(this.clientRepresentation.getId())
					.add(Collections.singletonList(roleRepresentation));
		}

	}

	/**
	 * This method is used to get all the roles for a client
	 * 
	 * @return list of RoleRepresentation
	 */
	public List<RoleRepresentation> getRoles() {
		List<RoleRepresentation> roleRepresentation = null;
		try {
			roleRepresentation = this.clientsResource.get(this.clientRepresentation.getId()).roles().list();
		} catch (Exception e) {
			logger.error(
					ErrorMessages.COULD_NOT_GET_CLIENT_ROLES.getErrorMessage() + this.clientRepresentation.getName(),
					e);
			throw e;
		}
		return roleRepresentation;

	}

	/**
	 * This method is used to get all the user for a client
	 * 
	 * @return list of userNames
	 */
	public List<String> allUsers() {
		logger.info("--> Entering get All Users()");
		List<UserRepresentation> users = null;
		try {
			logger.debug("Getting user list ");
			users = this.usersResource.list();
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_USER_LIST.getErrorMessage(), e);
			throw e;
		}
		List<String> userNames = new ArrayList<>();
		users.forEach(user -> userNames.add(user.getUsername()));
		logger.info("<-- Exiting get user List()");
		return userNames;
	}

	/**
	 * This method is used to get all the users and their roles
	 * 
	 * @return list of mapped user details
	 */
	public List<Map<String, Object>> getUsers() {
		logger.info("--> Entering getUsers()");
		List<Map<String, Object>> data = new ArrayList<>();
		List<UserRepresentation> reps = new ArrayList<>();
		try {
			reps = this.usersResource.list();
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_USER_LIST.getErrorMessage(), e);
			throw e;
		}
		for (UserRepresentation userRepresentation : reps) {
			Map<String, Object> map = new HashMap<>();
			if ((userRepresentation.getEmail() != null) && (!userRepresentation.getEmail().isEmpty())) {
				map.put("email", userRepresentation.getEmail());
				map.put("UserId", userRepresentation.getId());
				map.put("isEnabled", userRepresentation.isEnabled());
				List<String> role = new ArrayList<>();
				role = this.getClientRoleListForUser(userRepresentation.getId());
				map.put("role", role);
				data.add(map);
			}
		}
		logger.info("<-- Exiting getUsers()");
		return data;
	}

	public UserResource getUserResource(String userId) {
		logger.info("--> Entering getUserResource()");
		UserResource user = null;
		try {
			user = this.realmResource.users().get(userId);
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_USER_RESOURCE.getErrorMessage(), e);
			throw e;
		}
		logger.info("<-- Exiting getUserResource()");
		return user;
	}

	/**
	 * This method is used to get all the active sessions for a user
	 * 
	 * @param userId
	 * @return list of UserSessionRepresentation
	 */
	public List<UserSessionRepresentation> getAllActiveSessionsForUser(String userId) {
		List<UserSessionRepresentation> sessions = new ArrayList<>();
		logger.info("-->Entering getAllActiveSessionsForUser()");
		try {
			sessions = this.realmResource.users().get(userId).getUserSessions();
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_USER_ACTIVE_SESSIONS.getErrorMessage(), e);
			throw e;
		}
		return sessions;

	}

	/**
	 * This method is used to get all the offline sessions for a user
	 * 
	 * @param userId
	 * @return list of UserSessionRepresentation
	 */
	public List<UserSessionRepresentation> getAllUserOfflineSessions(String userId) {
		List<UserSessionRepresentation> sessions = null;
		try {
			sessions = this.clientsResource.get(this.clientRepresentation.getId()).getOfflineUserSessions(null, null);
		} catch (Exception e) {
			logger.error("Error while getting offline session count for client ", e);
		}
		return sessions;
	}

	/**
	 * This method is used to check for existing role
	 * 
	 * @param roleName
	 * @return true or false
	 */
	public boolean checkForExistingRole(String role) {
		boolean isRoelExists = false;
		RoleRepresentation roleRepresentation = null;
		try {
			logger.debug("Checking for role in keycloak for client:" + this.clientRepresentation.getName());
			roleRepresentation = this.clientsResource.get(this.clientRepresentation.getId()).roles().get(role)
					.toRepresentation();
		} catch (Exception ex) {
			logger.error("Error while getting role ", ex);
		}
		if (roleRepresentation != null && roleRepresentation.getName().equals(role)) {
			isRoelExists = true;
		}
		return isRoelExists;
	}

	/**
	 * This method is used to check for existing group
	 * 
	 * @param groupName
	 * @return true or false
	 */
	public boolean checkForExistingGroup(String groupName) {
		logger.info("--> Entering checkForExistingGroup()");
		boolean isGroupExists = false;
		List<GroupRepresentation> groups = new ArrayList<>();
		try {
			logger.debug("Checking for group in keycloak");
			groups = this.groupsResource.groups(groupName, null, null);
		} catch (Exception ex) {
			logger.error("Error getting group with name ", ex);
			logger.error("No group exists with name :: " + groupName);
		}
		if (groups.size() > 0) {
			GroupRepresentation group = this.groupsResource.groups().stream().filter(o -> o.getName().equals(groupName))
					.findFirst().get();
			if (group != null && group.getName().equals(groupName)) {
				isGroupExists = true;
			}
		}
		logger.info("<-- Exiting checkForExistingGroup()");
		return isGroupExists;
	}

	public void clearRealmCache() {
		try {
			this.keycloak.realm(this.keyCloakConfig.getRealm()).clearRealmCache();
		} catch (Exception e) {
			logger.error("Error while clearing realm cache ", e);
		}
	}

	public void clearUserCache() {
		try {
			this.keycloak.realm(this.keyCloakConfig.getRealm()).clearUserCache();
		} catch (Exception e) {
			logger.error("Error while clearing user cache ", e);
		}
	}

	public Map<String, Long> getClientOFFlineSessionCount() {
		logger.info("--> Entering getClientOFFlineSessionCount()");
		Map<String, Long> result = new HashMap<>();
		try {
			result = this.clientsResource.get(this.clientRepresentation.getId()).getOfflineSessionCount();
		} catch (Exception e) {
			logger.error("Error while getting offline session count for client ", e);
		}
		logger.info("<-- Exiting getClientOFFlineSessionCount()");
		return result;
	}

	public int getUserOFFlineSession(String userId) {
		logger.info("--> Entering getUserOFFlineSession()");
		List<UserSessionRepresentation> sessions = new ArrayList<>();
		int count = 0;
		try {
			sessions = this.usersResource.get(userId).getOfflineSessions(this.clientRepresentation.getId());
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_OFFLINE_SESSIONS.getErrorMessage(), e);
			throw e;
		}
		if (!sessions.isEmpty()) {
			count = sessions.size();
		}
		logger.debug("user's offline sessions =" + count);
		logger.info("--> Exiting getUserOFFlineSession()");
		return count;
	}

	public void addUserToRole(String userID, String role) {
		logger.info("--> Entering addUserToRole()");
		RoleRepresentation roleRepresentation = new RoleRepresentation();
		try {
			logger.debug("Getting roles for user ");
			roleRepresentation = this.clientsResource.get(this.clientRepresentation.getId()).roles().get(role)
					.toRepresentation();
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_ROLES.getErrorMessage() + userID, e);
			throw e;
		}
		try {
			logger.debug("Adding role to user ");
			this.usersResource.get(userID).roles().clientLevel(this.clientRepresentation.getId())
					.add(Collections.singletonList(roleRepresentation));
			logger.debug("Successfully added role to user");
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_ADD_ROLE_TO_USER.getErrorMessage() + userID + " role " + role, e);
			throw e;
		}
		logger.info("<-- Exiting addUserToRole()");

	}

	/**
	 * This method is used to check for existing user
	 * 
	 * @param emailId
	 * @return true or false
	 */
	public boolean checkForExistingUser(String emailId) {
		logger.info("--> Entering checkForExistingUser()");
		logger.debug("User emailId: " + emailId);
		boolean isUserExists = false;
		List<UserRepresentation> users = null;
		try {
			users = this.usersResource.search(null, null, null, emailId, null, null);
		} catch (Exception e) {
			logger.error("Error while user search ", e);
			throw new ServerErrorException(ErrorMessages.ERROR_FINDING_USER_WITH_EMAIL.getErrorMessage(),
					Response.Status.INTERNAL_SERVER_ERROR);
		}
		if (users != null && users.size() > 0) {
			UserRepresentation user = users.stream().filter(o -> o.getEmail().equals(emailId)).findFirst().get();
			if (user != null && user.getEmail().equals(emailId)) {
				isUserExists = true;
			}
		}
		logger.info("<-- Exiting findUserByEmail()");
		return isUserExists;
	}

	public int getOfflineSessionsFlagFromConfig() {
		return this.keyCloakConfig.getOfflineSessions();
	}

	public int getActiveSessionsFlagFromConfig() {
		return this.keyCloakConfig.getActiveSessions();
	}

	/**
	 * Create a group with given name
	 * 
	 * @param groupName
	 */
	public void createNewGroup(String groupName) {
		logger.info("Entering createNewGroup()");
		GroupRepresentation group = new GroupRepresentation();
		group.setName(groupName);
		try {
			logger.debug("Creating group with name " + groupName);
			this.realmResource.groups().add(group);
			logger.debug("Successfull created group");
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_CREATE_GROUP.getErrorMessage(), e);
			throw e;
		}
		logger.info("--> Exiting createNewGroup()");
	}

	/**
	 * To get All the groups in a realm
	 * 
	 * @return list of all groups
	 */
	public List<String> AllGroups() {
		logger.info("--> Entering get AllGroups()");
		GroupsResource groups = null;
		List<String> gropuNames = new ArrayList<>();
		try {
			logger.debug("Getting gropus for realm ");
			groups = this.realmResource.groups();
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_GROUPS.getErrorMessage(), e);
			throw e;
		}
		if (groups != null) {
			gropuNames = groups.groups().stream().map(i -> i.getName()).collect(Collectors.toList());
			logger.debug("successfully got names for groups");
		}
		logger.info("<-- Exiting get AllGroups()");
		return gropuNames;
	}

	/**
	 * To get group with given Name
	 * 
	 * @param groupName
	 * @return returns GroupRepresentation
	 */
	public GroupRepresentation getGroupWithName(String groupName) {
		logger.info("--> Entering getGroupWithName()");
		List<GroupRepresentation> groups = new ArrayList<>();
		GroupRepresentation representation = new GroupRepresentation();
		try {
			logger.debug("Checking for group with name " + groupName);
			groups = this.groupsResource.groups(groupName, null, null);
		} catch (Exception e) {
			logger.error(ErrorMessages.COULD_NOT_GET_GROUP_WITH_NAME + groupName, e);
			throw e;
		}
		representation = groups.stream().filter(o -> o.getName().equals(groupName)).findFirst().get();
		logger.info("<- Exiting getGroupWithName()");
		return representation;
	}

}
