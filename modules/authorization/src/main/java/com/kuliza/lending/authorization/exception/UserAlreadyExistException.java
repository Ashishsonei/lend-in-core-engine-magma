package com.kuliza.lending.authorization.exception;

public class UserAlreadyExistException extends RuntimeException {

	private static final long serialVersionUID = -2675569367532598957L;

	public UserAlreadyExistException(String message) {
		super(message);
	}
}
