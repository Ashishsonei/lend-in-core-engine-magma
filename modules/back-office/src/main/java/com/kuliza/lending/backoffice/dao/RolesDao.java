package com.kuliza.lending.backoffice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Roles;

@Repository
public interface RolesDao extends CrudRepository<Roles, Long> {

	public Roles findById(long id);

	public Roles findByIdAndIsDeleted(long id, boolean isDeleted);

	public Roles findByRoleName(String roleName);

	public Roles findByRoleNameAndIsDeleted(String roleName, boolean isDeleted);

}
