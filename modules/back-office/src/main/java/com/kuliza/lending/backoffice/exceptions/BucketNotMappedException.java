package com.kuliza.lending.backoffice.exceptions;

public class BucketNotMappedException extends RuntimeException {

	public BucketNotMappedException() {
		super();
	}

	public BucketNotMappedException(String message) {
		super(message);
	}

}
