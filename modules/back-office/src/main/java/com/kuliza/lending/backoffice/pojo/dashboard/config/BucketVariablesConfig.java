package com.kuliza.lending.backoffice.pojo.dashboard.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.kuliza.lending.backoffice.models.Buckets;

/***
 * This is a configuration class used for mapping of buckets with list of
 * variables
 * 
 * @author kuliza-330
 *
 */
public class BucketVariablesConfig {
	private String id;

	private String label;

	List<AppVariablesConfig> variables;

	public BucketVariablesConfig(BucketVariablesConfig bucket) {

		this.id = bucket.getId();
		this.label = bucket.getLabel();
		this.variables = bucket.getVariables();

	}

	public BucketVariablesConfig(Buckets bucket, Set<AppVariablesConfig> variables2) {

		this.id = bucket.getBucketKey();
		this.label = bucket.getLabel();
		this.variables = new ArrayList<>();
		for (AppVariablesConfig variable : variables2) {
			this.variables.add(variable);
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<AppVariablesConfig> getVariables() {
		return variables;
	}

	public void setVariables(List<AppVariablesConfig> variables) {
		this.variables = variables;
	}

}
