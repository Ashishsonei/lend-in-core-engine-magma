package com.kuliza.lending.backoffice.pojo.dashboard.config;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.kuliza.lending.backoffice.models.Variables;
import com.kuliza.lending.backoffice.utils.BackOfficeConstants;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({ @Type(value = VariablesOptionListConfig.class, name = BackOfficeConstants.VAR_TYPE_DROPDOWN),
		@Type(value = VariablesConfig.class, name = BackOfficeConstants.VAR_TYPE_STRING),
		@Type(value = VariablesConfig.class, name = BackOfficeConstants.VAR_TYPE_NUMBER) })
public class VariablesConfig {

	private String key;
	private String label;
	private boolean editable;
	private boolean writables;
	private String meta;
	private String type;
	private Object value;

	public VariablesConfig() {
		super();
	}

	public VariablesConfig(String key, String label, boolean editable, boolean writables, String meta, String type,
			Object value) {
		super();
		this.key = key;
		this.label = label;
		this.editable = editable;
		this.writables = writables;
		this.meta = meta;
		this.type = type;
		this.value = value;
	}

	public VariablesConfig(Variables variable, Object value) {
		super();
		this.key = variable.getMapping();
		this.label = variable.getLabel();
		this.editable = variable.isEditable();
		this.writables = variable.isWritable();
		this.meta = variable.getMeta();
		this.type = variable.getType();
		this.value = value;
	}

	public VariablesConfig(String label, String key, boolean editable, boolean writable, String meta, String type) {
		this.label = label;
		this.key = key;
		this.editable = editable;
		this.writables = writable;
		this.meta = meta;
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isWritables() {
		return writables;
	}

	public void setWritables(boolean writables) {
		this.writables = writables;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
