package com.kuliza.lending.backoffice.pojo.dashboard.configure;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.kuliza.lending.backoffice.utils.BackOfficeConstants;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({ @Type(value = VariableOptionListConfig.class, name = BackOfficeConstants.VAR_TYPE_DROPDOWN),
		@Type(value = VariableConfig.class, name = BackOfficeConstants.VAR_TYPE_STRING),
		@Type(value = VariableConfig.class, name = BackOfficeConstants.VAR_TYPE_NUMBER) })
public class VariableConfig {

	private String label;
	private String key;
	private boolean editable;
	private boolean writable;
	private String meta;
	private String type;

	public VariableConfig() {
		super();
	}

	public VariableConfig(String label, String key, boolean editable, boolean writable, String meta, String type) {
		super();
		this.label = label;
		this.key = key;
		this.editable = editable;
		this.writable = writable;
		this.meta = meta;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isWritable() {
		return writable;
	}

	public void setWritable(boolean writable) {
		this.writable = writable;
	}

}