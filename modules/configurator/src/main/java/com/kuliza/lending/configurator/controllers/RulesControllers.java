package com.kuliza.lending.configurator.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewRules;
import com.kuliza.lending.configurator.service.ruleservice.RuleServices;
import com.kuliza.lending.configurator.utils.Constants;

@RestController
@RequestMapping(value = "/api/product/{productId:^[1-9]+[0-9]*$}/group/{groupId:^[1-9]+[0-9]*}/rules")
public class RulesControllers {

	private static final Logger logger = LoggerFactory.getLogger(RulesControllers.class);

	@Autowired
	private RuleServices ruleServices;

	// API to Post New Rule or Update existing rules in a group
	@RequestMapping(method = RequestMethod.POST, value = "")
	public Object ruleSubmitOrUpdate(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "groupId") String groupId, @Valid @RequestBody SubmitNewRules input,
			BindingResult result, HttpServletRequest request) {
		logger.info("--> Entering rule submit or Update()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = ruleServices.validateSubmitRules(input, result, userId, productId, groupId);
			if (response == null) {
				logger.debug("creating new rule or updating existing rules for productId: " + productId + " groupId:"
						+ groupId);
				response = ruleServices.createNewRules(productId, groupId, input);
			}
			if (response.getStatus() == 200) {
				logger.debug("changes successfully done ");
				ruleServices.logSuccessResponse(request, userId, response, input);
			} else {
				ruleServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting rule submit or Update()");
			return response;
		} catch (Exception e) {
			return ruleServices.handleException(e, request, userId, input);
		}
	}

	// API to delete a Rule inside a group
	@RequestMapping(method = RequestMethod.DELETE, value = "/{ruleId:^[1-9]+[0-9]*$}")
	public Object deleteRule(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "groupId") String groupId, @PathVariable(value = "ruleId") String ruleId,
			HttpServletRequest request) {
		logger.info("--> Entering delete Rule()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = ruleServices.validateDeleteRule(userId, productId, groupId, ruleId);
			if (response == null) {
				logger.debug("Deleting rule for ruleId:" + ruleId);
				response = ruleServices.deleteRule(ruleId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully deleted rule for ruleId:" + ruleId);
				ruleServices.logSuccessResponse(request, userId, response);
			} else {
				ruleServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting delete Rule()");
			return response;
		} catch (Exception e) {
			return ruleServices.handleException(e, request, userId);
		}
	}

}
