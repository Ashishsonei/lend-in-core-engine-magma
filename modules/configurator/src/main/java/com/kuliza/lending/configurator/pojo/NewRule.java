package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

public class NewRule {

	@NotNull(message = "rules is a required key")
	@NotEmpty(message = "Rules List cannot be empty")
	@Valid
	private List<RuleDef> rules;

	@NotNull(message = "isActive is a required key")
	@Pattern(regexp = Constants.IS_BOOLEAN_REGEX, message = "Invalid is Active")
	private String isActive;

	@NotNull(message = "outputWeight is a required key")
	@Pattern(regexp = Constants.VALUE_REGEX, message = "Invalid Output Weight")
	private String outputWeight;

	@NotNull(message = "variableName is a required key")
	@Pattern(regexp = Constants.NAME_REGEX, message = Constants.INVALID_VARIABLE_NAME_MESSAGE)
	private String variableName;

	private String productId;

	public NewRule() {
		this.rules = new ArrayList<>();
		this.isActive = "";
		this.outputWeight = "";
		this.variableName = "";
	}

	public NewRule(List<RuleDef> rules, String isActive, String outputWeight, String variableName) {
		this.rules = rules;
		this.isActive = isActive;
		this.outputWeight = outputWeight;
		this.variableName = variableName;
	}

	public List<RuleDef> getRules() {
		return rules;
	}

	public void setRules(List<RuleDef> rules) {
		this.rules = rules;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getOutputWeight() {
		return outputWeight;
	}

	public void setOutputWeight(String outputWeight) {
		this.outputWeight = outputWeight;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("isActive : " + isActive + ", ");
	// inputData.append("outputWeight : " + outputWeight + ", ");
	// inputData.append("variableName : " + variableName + ", ");
	// inputData.append("rules : [ ");
	// for (int i = 0; i < rules.size(); i++) {
	// inputData.append(rules.get(i).toString());
	// if (i < rules.size() - 1) {
	// inputData.append(", ");
	// }
	// }
	// inputData.append("] }");
	// return inputData.toString();
	// }

}
