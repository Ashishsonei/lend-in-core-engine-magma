package com.kuliza.lending.configurator.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message.Level;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.conf.EqualityBehaviorOption;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.opencsv.CSVReader;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class HelperFunctions {

	private final static Logger logger = LoggerFactory.getLogger(HelperFunctions.class);

	private HelperFunctions() {
		throw new UnsupportedOperationException();
	}

	// extract variable name in expression from 'ID_<Id>'
	public static String makeReadableExpressionString(String expressionString, VariableDao variableDao,
			GroupDao groupDao, boolean flag) throws Exception {
		logger.debug("--> Entering makeReadableExpressionString() :: expressionString:" + expressionString);

		ArrayList<String> expressionList = new ArrayList<>(
				Arrays.asList(expressionString.split(Constants.EXPRESSION_TOKENIZER_REGEX)));
		StringBuilder newExpressionString = new StringBuilder();
		for (String item : expressionList) {
			item = item.trim();
			if (item.matches("^(ID_)[0-9]+$")) {
				int indexOfUnderScore = item.indexOf('_');
				long variableId = Long.parseLong(item.substring(indexOfUnderScore + 1));
				String expressionVariableName;
				// if flag==true, then IDs in expression string are variable IDs
				// else group IDs
				if (flag) {
					logger.debug("getting variable Name from variableId:"+variableId);
					Variable variable = variableDao.findByIdAndIsDeleted(variableId, false);
					expressionVariableName = variable.getName();
					logger.debug("variable Name is:"+expressionVariableName);
				} else {
					logger.debug("getting group Name from variableId:"+variableId);
					Group group = groupDao.findByIdAndIsDeleted(variableId, false);
					expressionVariableName = group.getName();
					logger.debug("group Name is:"+expressionVariableName);
				}

				newExpressionString.append(expressionVariableName + " ");
			} else {
				newExpressionString.append(item + " ");
			}
		}
		String readableExpressionString = newExpressionString.toString();
		// to remove extra space at the last character position
		readableExpressionString = readableExpressionString.substring(0, readableExpressionString.length() - 1);
		logger.debug("<-- Exiting makeReadableExpressionString() :: readableExpressionString:"+readableExpressionString);
		return readableExpressionString;
	}

	// store variables in expression like 'ID_<Id>' to differentiate variable Id
	// and numerical values in expression string.
	public static String makeStorableExpressionString(String expressionString, VariableDao variableDao,
			GroupDao groupDao, String productId, boolean flag) throws Exception {
		logger.debug("--> Entering makeStorableExpressionString():: expressionString:" + expressionString);
		ArrayList<String> expressionList = new ArrayList<>(
				Arrays.asList(expressionString.split(Constants.EXPRESSION_TOKENIZER_REGEX)));
		StringBuilder resultExpression = new StringBuilder();
		for (String item : expressionList) {
			if (item.matches(Constants.NAME_REGEX) && !item.matches(Constants.NOT_VARIABLE_NAMES_REGEX)) {
				StringBuilder newVariableName;
				String expressionVariableName = item;
				logger.debug("getting Expression variableName:" + expressionVariableName);
				if (flag) {
					Variable variable = variableDao.findByNameAndProductIdAndIsDeleted(expressionVariableName,
							Long.parseLong(productId), false);
					newVariableName = new StringBuilder("ID_");
					newVariableName.append(Long.toString(variable.getId()));
				} else {
					Group group = groupDao.findByNameAndProductIdAndIsDeleted(expressionVariableName,
							Long.parseLong(productId), false);
					newVariableName = new StringBuilder("ID_");
					newVariableName.append(Long.toString(group.getId()));
				}
				// '_' added to variable id to differentiate between a no. and
				// id in expression
				resultExpression.append(newVariableName.toString());
				logger.debug("appended variable to result expression::" + newVariableName.toString());
			} else if (!item.equals("")) {
				resultExpression.append(item);
			}
		}
		logger.debug("Result Expression After making to storable String ::" + resultExpression);
		logger.debug("<-- Exiting makeStorableExpressionString()");
		return resultExpression.toString();
	}

	// function to generate error json
	public static List<Map<String, String>> generateErrorResponseData(List<FieldError> errors) throws Exception {
		List<Map<String, String>> allErrorData = new ArrayList<>();
		for (FieldError error : errors) {
			Map<String, String> errorData = new HashMap<>();
			errorData.put("field", error.getField());
			errorData.put("message", error.getDefaultMessage());
			allErrorData.add(errorData);
		}
		return allErrorData;
	}

	// function to generate log string
	public static String generateLogString(String method, int statusCode, String url, String userId, Object data) {
		StringBuilder logString = new StringBuilder();
		logString.append(method.concat(" "));
		logString.append(Integer.toString(statusCode).concat(" "));
		logString.append(url.concat(" "));
		logString.append("UserId : ".concat(userId).concat(" "));
		logString.append("Data : ".concat(data.toString()));
		return logString.toString();
	}

	// function to generate log string
	public static String generateLogString(String method, String url, String userId, Object data) {
		StringBuilder logString = new StringBuilder();
		logString.append(method.concat(" "));
		logString.append(url.concat(" "));
		logString.append("UserId : ".concat(userId).concat(" "));
		logString.append("Data : ".concat(data.toString()));
		return logString.toString();
	}

	// function to generate data for log
	public static Object generateLogData(Object input, Object output) {
		Map<String, Object> logData = new HashMap<>();
		logData.put("input", input);
		logData.put("output", output);
		return logData;
	}

	// function to get stack trace for exception
	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	// function to create template for product
	@Transactional(rollbackFor = Exception.class)
	public static void createTemplate(String newProductId, String templateProductId, ProductDao productDao,
			ExpressionDao expressionDao, GroupDao groupDao, RuleDao ruleDao, VariableDao variableDao) throws Exception {
		logger.debug("--> Entering createTemplate()");
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(newProductId), false);
		List<Group> allGroups = groupDao.findByProductIdAndIsDeleted(Long.parseLong(templateProductId), false);
		List<Variable> allPrimitiveVariables = variableDao
				.findByProductIdAndCategoryAndIsDeleted(Long.parseLong(templateProductId), "Primitive", false);
		List<Variable> allDerivedVariables = variableDao
				.findByProductIdAndCategoryAndIsDeleted(Long.parseLong(templateProductId), "Derived", false);
		for (Variable variable : allPrimitiveVariables) {
			Variable newVariable = new Variable(variable.getName(), variable.getType(), variable.getSource(),
					variable.getCategory(), variable.getDescription(), "", product);
			variableDao.save(newVariable);
		}
		for (Variable variable : allDerivedVariables) {
			String expressionString = makeReadableExpressionString(variable.getExpression(), variableDao, groupDao,
					true);
			expressionString = makeStorableExpressionString(expressionString, variableDao, groupDao, newProductId,
					true);
			Variable newVariable = new Variable(variable.getName(), variable.getType(), variable.getSource(),
					variable.getCategory(), variable.getDescription(), expressionString, product);
			variableDao.save(newVariable);
		}

		for (Group group : allGroups) {
			Group newGroup = new Group(group.getName(), product);
			groupDao.save(newGroup);

			List<Expression> allGroupExpressions = expressionDao.findByGroupIdAndIsDeleted(group.getId(), false);
			for (Expression expression : allGroupExpressions) {
				String expressionString = makeReadableExpressionString(expression.getExpressionString(), variableDao,
						groupDao, true);
				expressionString = makeStorableExpressionString(expressionString, variableDao, groupDao, newProductId,
						true);
				Expression newExpression = new Expression(expressionString, true, false, null, newGroup);
				expressionDao.save(newExpression);
			}

			List<Rule> allGroupRules = ruleDao.findByGroupIdAndIsDeleted(group.getId(), false);
			for (Rule rule : allGroupRules) {
				Variable oldVariable = rule.getVariable();
				Variable newVariable = variableDao.findByNameAndProductIdAndIsDeleted(oldVariable.getName(),
						Long.parseLong(newProductId), false);
				Rule newRule = new Rule(rule.getInputRange(), rule.getOutputValue(), rule.getOutputWeight(),
						newVariable, rule.isActive(), newGroup);
				ruleDao.save(newRule);
			}
		}

		List<Expression> allProductExpressions = expressionDao
				.findByProductIdAndIsDeleted(Long.parseLong(templateProductId), false);
		for (Expression expression : allProductExpressions) {
			String expressionString = makeReadableExpressionString(expression.getExpressionString(), variableDao,
					groupDao, false);
			expressionString = makeStorableExpressionString(expressionString, variableDao, groupDao, newProductId,
					false);
			Expression newExpression = new Expression(expressionString, false, true, product, null);
			expressionDao.save(newExpression);
		}
	logger.debug("<-- Exiting createTemplate()");
	}

	// function to generate rules String
	public static String getGeneratedRulesString(long groupId, RuleDao ruleDao, String productFolderName,
			String groupFolderName) throws Exception {
		List<Rule> allRules = ruleDao.findByGroupIdAndIsDeletedAndIsActive(groupId, false, true);
		StringBuilder result = new StringBuilder();
		long salienceCounter = 100000;
		result.append("package rules." + productFolderName + "." + groupFolderName + ";\n\n");
		result.append("import com.kuliza.lending.configurator.pojo.DroolsInput;\n\n");
		for (Rule rule : allRules) {
			result.append("rule");
			result.append("\n'Rule Number : " + rule.getId() + "'\n");
			result.append("salience " + salienceCounter--);
			result.append("\nwhen\n");
			result.append("$di : DroolsInput (");
			Variable variable = rule.getVariable();
			if (variable.getType().equals("Numerical")) {
				result.append("Double.parseDouble(inputValues.get('" + variable.getName() + "').toString()) "
						+ rule.getInputRange());
			} else if (variable.getType().equals("String")) {
				if (rule.getInputRange().startsWith("==")) {
					result.append("inputValues.get('" + variable.getName() + "').toString().equals(\""
							+ rule.getInputRange().replaceAll("=", "").replaceAll("!", "").trim() + "\")");
				} else {
					result.append("!inputValues.get('" + variable.getName() + "').toString().equals(\""
							+ rule.getInputRange().replaceAll("=", "").replaceAll("!", "").trim() + "\")");
				}
			} else {
				result.append("Boolean.parseBoolean(inputValues.get('" + variable.getName() + "').toString()) "
						+ rule.getInputRange());
			}
			result.append(")\n");
			result.append("then\n");
			if (rule.getOutputValue().matches(Constants.VALUE_REGEX)) {
				result.append("$di.setSingleOutputScore('" + variable.getName() + "', "
						+ Double.parseDouble(rule.getOutputValue()) * rule.getOutputWeight() + ");\n");
			} else {
				result.append("$di.setSingleOutputScore('" + variable.getName() + "', "
						+ Boolean.parseBoolean(rule.getOutputValue()) + ");\n");
			}
			result.append("end\n\n");
		}
		return result.toString();
	}

	@SuppressFBWarnings({ "RV_RETURN_VALUE_IGNORED_BAD_PRACTICE", "DM_DEFAULT_ENCODING", "DLS_DEAD_LOCAL_STORE" })
	public static KieContainer generateRulesDRLFile(String productId, ProductDao productDao, GroupDao groupDao,
			RuleDao ruleDao) throws Exception {
		List<Group> allGroups = groupDao.findByProductId(Long.parseLong(productId));
		String productFolderName = "Product_" + productId;
		for (Group group : allGroups) {
			String groupFolderName = "Rules_" + group.getName() + "_" + Long.toString(group.getId());
			String drl = getGeneratedRulesString(group.getId(), ruleDao, productFolderName, groupFolderName);
			// create new file
			URL fileUrl = CreditEngineApplication.class.getResource("/application.properties");
			int index = fileUrl.getPath().lastIndexOf("/");
			if (index != -1) {
				String path = fileUrl.getPath().substring(0, index);
				File file = new File(
						path + "/rules/" + productFolderName + "/" + groupFolderName + "/" + groupFolderName + ".drl");
				file.getParentFile().mkdirs();
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.append(drl);
				bw.close();
			}

		}
		return buildKieContainerAgain(Constants.RULES_PATH + productFolderName + "/");
	}

	public static String mapToString(Map<String, Object> inputMap) {
		StringBuilder mapString = new StringBuilder("{ ");
		Iterator<Map.Entry<String, Object>> it = inputMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pair = it.next();
			mapString.append(pair.getKey() + " : " + pair.getValue().toString());
			if (it.hasNext()) {
				mapString.append(", ");
			}
		}
		mapString.append(" }");
		return mapString.toString();
	}

	public static Resource[] getRuleFiles(String rulePath) throws IOException {
		ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
		URL fileUrl = CreditEngineApplication.class.getResource("/application.properties");
		int index = fileUrl.getPath().lastIndexOf("/");
		String path = "";
		if (index != -1) {
			path = fileUrl.getPath().substring(0, index);
		}
		return resourcePatternResolver.getResources("file:" + path + "/" + rulePath + "**/*.drl");

	}

	public static KieContainer buildKieContainerAgain(String rulePath) throws FileNotFoundException, IOException {
		KieServices kieServices = KieServices.Factory.get();
		KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
		KieModuleModel kieModuleModel = kieServices.newKieModuleModel();
		for (Resource file : getRuleFiles(rulePath)) {
			String[] filePath = file.getURI().toString().split("/");
			KieBaseModel kieBaseModel1 = kieModuleModel.newKieBaseModel(filePath[filePath.length - 2]).setDefault(true)
					.setEqualsBehavior(EqualityBehaviorOption.EQUALITY)
					.setEventProcessingMode(EventProcessingOption.STREAM);
			kieBaseModel1.newKieSessionModel("session_" + filePath[filePath.length - 2]).setDefault(true)
					.setType(KieSessionModel.KieSessionType.STATEFUL).setClockType(ClockTypeOption.get("realtime"));
			kieBaseModel1.addPackage("rules." + filePath[filePath.length - 3] + "." + filePath[filePath.length - 2]);
			FileInputStream fis = new FileInputStream(file.getFile());
			kieFileSystem.write("src/main/resources/rules/" + filePath[filePath.length - 3] + "/"
					+ filePath[filePath.length - 2] + "/" + file.getFilename(),
					kieServices.getResources().newInputStreamResource(fis));
		}
		kieFileSystem.writeKModuleXML(kieModuleModel.toXML());
		KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
		kieBuilder.buildAll();
		if (kieBuilder.getResults().hasMessages(Level.ERROR)) {
			throw new RuntimeException("Build Errors:\n" + kieBuilder.getResults().toString());
		}
		return kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
	}

	public static void writeToCSV(Writer writer, List<Map<String, Object>> myArrList, List<String> headers,
			List<String> dataName) throws IOException {
		for (int k = 0; k < headers.size(); k++) {
			writer.write(headers.get(k).toString());
			if (k != headers.size() - 1) {
				writer.write(",");
			}
		}
		writer.write("\r\n");
		for (int i = 0; i < myArrList.size(); i++) {
			for (int j = 0; j < dataName.size(); j++) {
				writer.write(String.valueOf(myArrList.get(i).get(dataName.get(j))));
				if (j != dataName.size() - 1) {
					writer.write(",");
				}
			}
			writer.write("\r\n");
		}
	}

	public static void generateResponseXls(File temp, HttpServletResponse response, String FileName) throws Exception {
		OutputStream out = response.getOutputStream();
		try {
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + FileName);
			SXSSFSheet sheet = null;
			CSVReader reader = null;
			Workbook workBook = null;
			String[] nextLine;
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(temp), "UTF-8"));
			reader = new CSVReader(br);
			workBook = new SXSSFWorkbook();
			sheet = (SXSSFSheet) workBook.createSheet("Sheet");
			int rowNum = 0;
			while ((nextLine = reader.readNext()) != null) {
				Row currentRow = sheet.createRow(rowNum++);
				for (int i = 0; i < nextLine.length; i++) {
					currentRow.createCell(i).setCellValue(nextLine[i].toString());
				}
			}
			reader.close();
			workBook.write(out);
			workBook.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.close();
		}
	}
}
