package com.kuliza.lending.journey.customer_dashboard.pojo;

import java.util.List;
import java.util.Map;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Enums;
import com.kuliza.lending.journey.model.LOSUserAddressModel;

public class CustomerDashboardAddressInfo {

	private String permanentAddressLine1;
	private String permanentAddressLine2;
	private String permanentCityDistrict;
	private String permanentProvince;

	private String temporaryAddressLine1;
	private String temporaryAddressLine2;
	private String temporaryCityDistrict;
	private String temporaryProvince;

	public CustomerDashboardAddressInfo() {
		super();
		this.permanentAddressLine1 = "";
		this.permanentAddressLine2 = "";
		this.permanentCityDistrict = "";
		this.permanentProvince = "";
		this.temporaryAddressLine1 = "";
		this.temporaryAddressLine2 = "";
		this.temporaryCityDistrict = "";
		this.temporaryProvince = "";
	}

	public CustomerDashboardAddressInfo(List<LOSUserAddressModel> losUserAddressModelList) {
		super();
		for (LOSUserAddressModel losUserAddressModel : losUserAddressModelList) {
			if (losUserAddressModel.getAddressType() != null) {
				if (losUserAddressModel.getAddressType().equals(Enums.AddressType.PERMANENT.toString())) {
					this.permanentAddressLine1 = CommonHelperFunctions
							.getStringValue(losUserAddressModel.getAddressLine1());
					this.permanentAddressLine2 = CommonHelperFunctions
							.getStringValue(losUserAddressModel.getAddressLine2());
					this.permanentCityDistrict = CommonHelperFunctions
							.getStringValue(losUserAddressModel.getCityDistrict());
					this.permanentProvince = CommonHelperFunctions.getStringValue(losUserAddressModel.getState());
				}
				if (losUserAddressModel.getAddressType().equals(Enums.AddressType.CURRENT.toString())) {
					this.temporaryAddressLine1 = CommonHelperFunctions
							.getStringValue(losUserAddressModel.getAddressLine1());
					this.temporaryAddressLine2 = CommonHelperFunctions
							.getStringValue(losUserAddressModel.getAddressLine2());
					this.temporaryCityDistrict = CommonHelperFunctions
							.getStringValue(losUserAddressModel.getCityDistrict());
					this.temporaryProvince = CommonHelperFunctions.getStringValue(losUserAddressModel.getState());
				}
			}
		}
	}

	public CustomerDashboardAddressInfo(String permanentAddressLine1, String permanentAddressLine2,
			String permanentCityDistrict, String permanentProvince, String temporaryAddressLine1,
			String temporaryAddressLine2, String temporaryCityDistrict, String temporaryProvince) {
		super();
		this.permanentAddressLine1 = permanentAddressLine1;
		this.permanentAddressLine2 = permanentAddressLine2;
		this.permanentCityDistrict = permanentCityDistrict;
		this.permanentProvince = permanentProvince;
		this.temporaryAddressLine1 = temporaryAddressLine1;
		this.temporaryAddressLine2 = temporaryAddressLine2;
		this.temporaryCityDistrict = temporaryCityDistrict;
		this.temporaryProvince = temporaryProvince;
	}

	public CustomerDashboardAddressInfo(Map<String, Object> data) {
		super();
		if (!CommonHelperFunctions
				.getBooleanValue(data.get(Constants.TEMPORARY_ADDRESS_SAME_AS_PERMANENT_ADDRESS_KEY))) {
			this.temporaryAddressLine1 = CommonHelperFunctions
					.getStringValue(data.get(Constants.TEMPORARY_ADDRESS_LINE_1_KEY));
			this.temporaryAddressLine2 = CommonHelperFunctions
					.getStringValue(data.get(Constants.TEMPORARY_ADDRESS_LINE_2_KEY));
			this.temporaryCityDistrict = CommonHelperFunctions
					.getStringValue(data.get(Constants.TEMPORARY_CITY_DISTRICT_KEY));
			this.temporaryProvince = CommonHelperFunctions.getStringValue(data.get(Constants.TEMPORARY_PROVINCE_KEY));
		} else {
			this.temporaryAddressLine1 = CommonHelperFunctions
					.getStringValue(data.get(Constants.PERMANENT_ADDRESS_LINE_1_KEY));
			this.temporaryAddressLine2 = CommonHelperFunctions
					.getStringValue(data.get(Constants.PERMANENT_ADDRESS_LINE_2_KEY));
			this.temporaryCityDistrict = CommonHelperFunctions
					.getStringValue(data.get(Constants.PERMANENT_CITY_DISTRICT_KEY));
			this.temporaryProvince = CommonHelperFunctions.getStringValue(data.get(Constants.PERMANENT_PROVINCE_KEY));
		}

		this.permanentAddressLine1 = CommonHelperFunctions
				.getStringValue(data.get(Constants.PERMANENT_ADDRESS_LINE_1_KEY));
		this.permanentAddressLine2 = CommonHelperFunctions
				.getStringValue(data.get(Constants.PERMANENT_ADDRESS_LINE_2_KEY));
		this.permanentCityDistrict = CommonHelperFunctions
				.getStringValue(data.get(Constants.PERMANENT_CITY_DISTRICT_KEY));
		this.permanentProvince = CommonHelperFunctions.getStringValue(data.get(Constants.PERMANENT_PROVINCE_KEY));
	}

	public String getPermanentAddressLine1() {
		return permanentAddressLine1;
	}

	public void setPermanentAddressLine1(String permanentAddressLine1) {
		this.permanentAddressLine1 = permanentAddressLine1;
	}

	public String getPermanentAddressLine2() {
		return permanentAddressLine2;
	}

	public void setPermanentAddressLine2(String permanentAddressLine2) {
		this.permanentAddressLine2 = permanentAddressLine2;
	}

	public String getPermanentCityDistrict() {
		return permanentCityDistrict;
	}

	public void setPermanentCityDistrict(String permanentCityDistrict) {
		this.permanentCityDistrict = permanentCityDistrict;
	}

	public String getPermanentProvince() {
		return permanentProvince;
	}

	public void setPermanentProvince(String permanentProvince) {
		this.permanentProvince = permanentProvince;
	}

	public String getTemporaryAddressLine1() {
		return temporaryAddressLine1;
	}

	public void setTemporaryAddressLine1(String temporaryAddressLine1) {
		this.temporaryAddressLine1 = temporaryAddressLine1;
	}

	public String getTemporaryAddressLine2() {
		return temporaryAddressLine2;
	}

	public void setTemporaryAddressLine2(String temporaryAddressLine2) {
		this.temporaryAddressLine2 = temporaryAddressLine2;
	}

	public String getTemporaryCityDistrict() {
		return temporaryCityDistrict;
	}

	public void setTemporaryCityDistrict(String temporaryCityDistrict) {
		this.temporaryCityDistrict = temporaryCityDistrict;
	}

	public String getTemporaryProvince() {
		return temporaryProvince;
	}

	public void setTemporaryProvince(String temporaryProvince) {
		this.temporaryProvince = temporaryProvince;
	}

}
