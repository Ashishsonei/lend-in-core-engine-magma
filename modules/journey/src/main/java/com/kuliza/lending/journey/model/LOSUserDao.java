package com.kuliza.lending.journey.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface LOSUserDao extends CrudRepository<LOSUserModel, Long>, JpaRepository<LOSUserModel, Long> {

	public LOSUserModel findById(long id);

	public LOSUserModel findByIdAndIsDeleted(long id, boolean isDeleted);

	public LOSUserModel findByContactNumberAndIsDeleted(String contactNumber, boolean isDeleted);

}
