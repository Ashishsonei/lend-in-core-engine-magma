package com.kuliza.lending.journey.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CustomerLoginInput {

	@NotNull
	@Size(min = 9, max = 11)
	@Pattern(regexp = "^(([1-9])[0-9]{8,9})$")
	String mobile;

	public CustomerLoginInput() {
		super();
	}

	public CustomerLoginInput(String mobile) {
		super();
		this.mobile = mobile;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
